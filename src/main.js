import Vue from "vue";
import Vuex from "vuex";
import App from "./App.vue";
import store from './store/store'

Vue.use(Vuex);

Vue.config.productionTip = false;
Vue.config.errorHandler = err => {
  store.commit("SET_TOAST_ERROR_MSG", err);
  store.dispatch("displayToast", {isError: true});
}


new Vue({
  render: h => h(App),
  store
}).$mount("#app");
