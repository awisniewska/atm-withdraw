const state = {
    showToast: false,
    timeoutRunning: false,
    showErrorToast: false,
    toastError: ""
}

const mutations = {
    SET_SHOW_TOAST(state, data){
        state.showToast = data;
    },
    SET_ERROR_TOAST(state, data){
        state.showErrorToast = data;
    },
    SET_TOAST_ERROR_MSG(state, data){
        state.toastError = data;
    }
}

const actions = {
    displayToast({}, {isError, time}) {
        isError ? state.showErrorToast = true : state.showToast = true;
        if (!time) time = 2000
        if (!state.timeoutRunning) {
            setTimeout(() => { isError ? state.showErrorToast = false : state.showToast = false;
                               state.timeoutRunning = false;
                            }, time)
            state.timeoutRunning = true
        }
    }
}

const getters = {
    getDisplayToast(state) {
        return state.showToast
    },
    getDisplayErrorToast(state){
        return state.showErrorToast;
    },
    getToastErrorMsg(state){
        return state.toastError;
    }
}

export default {
    state,
    mutations,
    actions,
    getters
}
